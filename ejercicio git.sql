﻿-- Listar el nombre y la edad de los ciclista que hayan ganado puertos con una altura inferiror a 1500 y una edad entre 19 y 30 años.
SELECT c.nombre, c.edad FROM ciclista c INNER JOIN puerto p USING(dorsal) WHERE p.altura<1500 AND (c.edad>=19 AND c.edad<=30);

SELECT c.nombre, c.edad FROM ciclista c WHERE dorsal IN (SELECT p.dorsal FROM puerto p WHERE p.altura<1500) AND (c.edad>=19 AND c.edad<=30);